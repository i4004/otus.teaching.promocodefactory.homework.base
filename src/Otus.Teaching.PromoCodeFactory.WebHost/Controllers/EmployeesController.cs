﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Employees;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Roles;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
	/// <summary>
	/// Сотрудники
	/// </summary>
	[ApiController]
	[Route("api/v1/[controller]")]
	public class EmployeesController
		: ControllerBase
	{
		private readonly IRepository<Employee> _employeeRepository;

		public EmployeesController(IRepository<Employee> employeeRepository)
		{
			_employeeRepository = employeeRepository;
		}

		/// <summary>
		/// Получить данные всех сотрудников
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public async Task<List<EmployeeShortViewModel>> GetEmployeesAsync()
		{
			var employees = await _employeeRepository.GetAllAsync();

			var employeesModelList = employees.Select(x =>
				new EmployeeShortViewModel()
				{
					Id = x.Id,
					Email = x.Email,
					FullName = x.FullName,
				}).ToList();

			return employeesModelList;
		}

		/// <summary>
		/// Получить данные сотрудника по Id
		/// </summary>
		/// <returns></returns>
		[HttpGet("{id:guid}")]
		public async Task<ActionResult<EmployeeViewModel>> GetEmployeeByIdAsync(Guid id)
		{
			var employee = await _employeeRepository.GetByIdAsync(id);

			if (employee == null)
				return NotFound();

			var employeeModel = new EmployeeViewModel()
			{
				Id = employee.Id,
				Email = employee.Email,
				Roles = employee.Roles.Select(x => new RoleViewModel()
				{
					Name = x.Name,
					Description = x.Description
				}).ToList(),
				FullName = employee.FullName,
				AppliedPromocodesCount = employee.AppliedPromocodesCount
			};

			return employeeModel;
		}

		/// <summary>
		/// Creates the employee
		/// </summary>
		[HttpPost]
		public async Task<ActionResult> Create([FromBody] EmployeeCreateViewModel model)
		{
			await _employeeRepository.Create(new Employee
			{
				Id = Guid.NewGuid(),
				FirstName = model.FirstName,
				LastName = model.LastName,
				Email = model.Email
			});

			return NoContent();
		}

		/// <summary>
		/// Updates the employee
		/// </summary>
		[HttpPut]
		public async Task<ActionResult> Update([FromBody] EmployeeUpdateViewModel model)
		{
			var item = await _employeeRepository.GetByIdAsync(model.Id);

			if (item == null)
				return UnprocessableEntity();

			item.FirstName = model.FirstName;
			item.LastName = model.LastName;
			item.Email = model.Email;

			await _employeeRepository.Update(item);

			return NoContent();
		}

		/// <summary>
		/// Deletes the employee
		/// </summary>
		[HttpDelete]
		public async Task<ActionResult> Delete(Guid ID)
		{
			var item = await _employeeRepository.GetByIdAsync(ID);

			if (item == null)
				return UnprocessableEntity();

			await _employeeRepository.Delete(item);

			return NoContent();
		}
	}
}