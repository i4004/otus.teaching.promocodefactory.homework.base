﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Roles
{
	public class RoleViewModel
		: BaseEntity
	{
		public string Name { get; set; }

		public string Description { get; set; }
	}
}