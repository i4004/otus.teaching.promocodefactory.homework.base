﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Roles;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Employees
{
	public class EmployeeViewModel
	{
		public Guid Id { get; set; }
		public string FullName { get; set; }

		public string Email { get; set; }

		public List<RoleViewModel> Roles { get; set; }

		public int AppliedPromocodesCount { get; set; }
	}
}