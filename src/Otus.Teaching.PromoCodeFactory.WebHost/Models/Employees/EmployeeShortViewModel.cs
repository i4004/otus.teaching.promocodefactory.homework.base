﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Employees
{
	public class EmployeeShortViewModel
	{
		public Guid Id { get; set; }

		public string FullName { get; set; }

		public string Email { get; set; }
	}
}