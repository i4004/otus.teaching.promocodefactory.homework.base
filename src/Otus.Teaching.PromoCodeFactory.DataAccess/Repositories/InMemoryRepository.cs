﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public class InMemoryRepository<T>
		: IRepository<T>
		where T : BaseEntity
	{
		protected IList<T> Data { get; set; }

		public InMemoryRepository(IEnumerable<T> data)
		{
			Data = data.ToList();
		}

		public Task<IEnumerable<T>> GetAllAsync()
		{
			return Task.FromResult((IEnumerable<T>)Data);
		}

		public Task<T> GetByIdAsync(Guid id)
		{
			return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
		}

		public Task Create(T item)
		{
			Data.Add(item);

			return Task.CompletedTask;
		}

		public Task Delete(T item)
		{
			if (!Data.Remove(item))
				throw new InvalidOperationException("Item not found in the list");

			return Task.CompletedTask;
		}

		public Task Update(T item)
		{
			var itemIndex = Data.IndexOf(item);

			if (itemIndex == -1)
				throw new InvalidOperationException("Item not found in the list");

			Data[itemIndex] = item;

			return Task.CompletedTask;
		}
	}
}